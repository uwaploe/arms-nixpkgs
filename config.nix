with import <nixpkgs> {};
let
  commonPaths = [
    glib
    hdf5
    libtool
    libusb1
    pkgconfig
    python27Full
    python27Packages.python
    python27Packages.attrs
    python27Packages.cffi
    python27Packages.h5py
    python27Packages.numpy
    python27Packages.pip
    python27Packages.pkgconfig
    python27Packages.pyusb
    python27Packages.pyxattr
    python27Packages.scipy
    python27Packages.setuptools
    python27Packages.six
    python27Packages.virtualenv
    tree
    libuuid
  ];
  armsPaths = [
    gs24dsi-dev
    gs24dsi-utils
    libgsadc-nowrc
    runit
  ];
  ivarPaths = [
    gs24dsi-dev
    gs24dsi-utils
    libgsadc-nowrc
  ];
in {
  packageOverrides = pkgs: rec {
    arms = buildEnv {
      name = "all-arms";
      paths = with pkgs; commonPaths ++ armsPaths;
    };

    ivar = buildEnv {
      name = "all-ivar";
      paths = with pkgs; commonPaths ++ ivarPaths;
    };

    gs24dsi-utils = callPackage ./pkgs/gs24dsi-utils { };
    gs24dsi-dev = callPackage ./pkgs/gs24dsi-dev { };
    gs24dsi16wrc-utils = callPackage ./pkgs/gs24dsi16wrc-utils { };
    gs24dsi16wrc-dev = callPackage ./pkgs/gs24dsi16wrc-dev { };
    libaiousb = callPackage ./pkgs/libaiousb {
      pythonSupport = true;
      python = pkgs.python27;
      swig = pkgs.swig;
    };
    libgsadc = callPackage ./pkgs/libgsadc/default.nix {
      inherit gs24dsi16wrc-dev;
      gs24dsi-dev = null;
    };
    libgsadc-beta = callPackage ./pkgs/libgsadc/burstmode.nix {
      inherit gs24dsi16wrc-dev;
      gs24dsi-dev = null;
    };
    libgsadc-nowrc = callPackage ./pkgs/libgsadc/default.nix {
      inherit gs24dsi-dev;
      gs24dsi16wrc-dev = null;
      noWRC = true;
    };
  };
}