# ARMS Software Packages

This repository contains a collection of
required [NixOS](http://nixos.org) software packages for the user account,
*sysop*, on the ARMS (and IVAR) System Interface Controllers.

## Installation

Log-in to the *sysop* account and clone this repository to the
`~/.nixpkgs` directory then run `nix-env` to install the software:

``` shellsession
git clone https://bitbucket.org/uwaploe/arms-nixpkgs.git .nixpkgs
nix-env -r -i all-arms
```

On the IVAR system, substitute `all-ivar` for `all-arms`.
