{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };
  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  self = rec {
    dscud = callPackage ./pkgs/dscud {
      kernel = pkgs.linux;
    };
    gs24dsi-utils = callPackage ./pkgs/gs24dsi-utils { };
    gs24dsi-dev = callPackage ./pkgs/gs24dsi-dev { };
    gs24dsi16wrc-utils = callPackage ./pkgs/gs24dsi16wrc-utils { };
    gs24dsi16wrc-dev = callPackage ./pkgs/gs24dsi16wrc-dev { };
    libaiousb = callPackage ./pkgs/libaiousb {
      pythonSupport = true;
      python = pkgs.python27;
      swig = pkgs.swig;
    };
    libgsadc = callPackage ./pkgs/libgsadc/default.nix {
      inherit gs24dsi16wrc-dev;
      gs24dsi-dev = null;
    };
    libgsadc_1_3_10 = callPackage ./pkgs/libgsadc/oldversion.nix {
      inherit gs24dsi16wrc-dev;
      gs24dsi-dev = null;
    };
    libgsadc-beta = callPackage ./pkgs/libgsadc/burstmode.nix {
      inherit gs24dsi16wrc-dev;
      gs24dsi-dev = null;
    };
    libgsadc-nowrc = callPackage ./pkgs/libgsadc/default.nix {
      inherit gs24dsi-dev;
      gs24dsi16wrc-dev = null;
      noWRC = true;
    };
    libgsadc-nowrc_1_3_10 = callPackage ./pkgs/libgsadc/oldversion.nix {
      inherit gs24dsi-dev;
      gs24dsi16wrc-dev = null;
      noWRC = true;
    };
    libmorayadc = callPackage ./pkgs/libmorayadc { };
    libmorayadc-wrc = callPackage ./pkgs/libmorayadc-wrc { };
  };
in self
