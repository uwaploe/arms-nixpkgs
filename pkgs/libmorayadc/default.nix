{ stdenv, fetchurl, pkgconfig, libtool, gs24dsi-dev, glib, dbus_glib, libuuid }:

let
  version = "1.2.1";
  tarball = "release-${version}.tar.gz";
  repo = "mfkenney/moray-adc";
in
stdenv.mkDerivation {
  name = "libmorayadc-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "0hqgflhxxbaqn9q4sqj71wyzh7448g90ncmr46vdf4inlj9zsphv";
  };
  nativeBuildInputs = [ pkgconfig ];

  buildInputs = [
    libtool
    gs24dsi-dev
    glib
    dbus_glib
    libuuid
  ];

  propagatedBuildInputs = [
    gs24dsi-dev
    glib
    dbus_glib
    libuuid
  ];

  preBuild = ''
    make clean
  '';

  installPhase = ''
    make prefix=$out install-lib
   '';

  meta = {
    description = "Library for MORAY Project A/D system";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
