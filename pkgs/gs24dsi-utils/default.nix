{ stdenv, fetchurl, glibc }:

let
  version = "4.3.65.13.0";
  repo = "uwaploe/gs24dsi";
  tarball = "v${version}.tar.gz";
in
stdenv.mkDerivation {
  name = "gs24dsi-utils-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "0fg0vizizkq8my7i56dwdgni4j94mn9bjhvbdrq0xpv3cvilx6hk";
  };

  buildInputs = [ glibc ];

  patches = [
    ./fix-makefiles.patch
  ];

  installPhase = ''
    make prefix=$out install
  '';

  meta = {
    description = "Utility programs for GS 24DSI A/D boards";
    homepage = "http://www.generalstandards.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
