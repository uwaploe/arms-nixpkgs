{ stdenv, fetchurl, glibc}:

let
  version = "2.0.60.8.0";
  repo = "uwaploe/gs24dsi16wrc";
  tarball = "v${version}.tar.gz";
in
stdenv.mkDerivation {
  name = "gs24dsi16wrc-dev-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "1f99fbb1mj5qhjgn28lv64qy8mnq1f0g53xhhgr21y873pmzf9yw";
  };

  buildInputs = [ glibc ];

  patches = [
    ./fix-makefiles.patch
    ./quiet-mode.patch
    ./enable-autocal.patch
  ];

  installPhase = ''
    make prefix=$out install-lib
    cp driver/24dsi16wrc.h driver/*_common.h driver/gsc_pci*.h $out/include
  '';

  meta = {
    description = "Libraries and C header files for GS 24DSI A/D boards";
    homepage = "http://www.generalstandards.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
