{ stdenv, fetchurl, cmake, libusb1
, python ? null
, swig ? null
, pythonSupport ? false
}:

assert pythonSupport -> swig != null && python != null;

let
  version = "1.116.1";
in
stdenv.mkDerivation {
  name = "libaiousb-${version}";
  src = fetchurl {
    url = "http://accesio.com/files/packages/AIOUSB-Latest.tar.gz";
    sha256 = "1kz6lqbvqr74z0k2fy71v0a5vg9riqdadg8zrp9ajn6q4ziri9rx";
  };

  patches = [
    ./pkgconfig.patch
    ./pyinstall.patch
    ./rpath.patch
    ./product_id.patch
  ];

  buildInputs = [cmake libusb1]
    ++ stdenv.lib.optionals pythonSupport [swig python];

  propagatedBuildInputs = [
    libusb1
  ];

  preConfigure = ''
    cd ./AIOUSB
  '';

  buildPhase = ''
    cmake -DCMAKE_INSTALL_PREFIX=$out ${stdenv.lib.optionalString pythonSupport "-DBUILD_PYTHON=ON"} ..
    make
  '';

  installPhase = ''
    make install
  '';

  # I could not reliably include the path for libaiousb in the Python
  # module via the Cmake build, hence the hack below.
  postFixup = ''
      module=$out/lib/python2.7/site-packages/_AIOUSB.so
      rpath=$out/lib
      if [[ -e $module ]]; then
          echo "patching RPATH in $module"
          oldrpath=$(patchelf --print-rpath $module)
          patchelf --set-rpath "''${oldrpath:+$oldrpath:}$rpath" $module
      fi
  '';

  meta = {
    description = "Library for the Acces I/O USB data acquisition boards";
    homepage = "http://accesio.com/";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
