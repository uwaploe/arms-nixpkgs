{ stdenv, fetchurl, pkgconfig, libtool, gs24dsi16wrc-dev, glib, libuuid }:

let
  version = "1.5.1";
  tarball = "release-${version}.tar.gz";
  repo = "mfkenney/moray-adc";
in
stdenv.mkDerivation {
  name = "libmorayadc-wrc-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "1a3i6j37688ixr7r2k6ws7j1ircdx53x9zd6aagpxlsbdmk0p34j";
  };
  nativeBuildInputs = [ pkgconfig ];

  buildInputs = [
    libtool
    gs24dsi16wrc-dev
    glib
    libuuid
  ];

  propagatedBuildInputs = [
    gs24dsi16wrc-dev
    glib
    libuuid
  ];

  preBuild = ''
    make clean
  '';

  installPhase = ''
    make prefix=$out install-lib
   '';

  meta = {
    description = "Library for MORAY Project A/D system (WRC variant)";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
