{ stdenv, fetchurl, kernel, unzip, bash }:

## Usage
# This package needs to be added to both the udev package
# list and the kernel modules list
#    boot.extraModulePackages = [ pkgs.dscud ];
#    services.udev.packages = [ pkgs.dscud ];
#
# The module name (not package name) needs to be added
# to the list of modules loaded at boot time:
#    boot.kernelModules = [ "dscudkp" ];
#
let
  version = "7.00";
  archive = "dscud-${version}.zip";
  kd = "${kernel.dev}/lib/modules/${kernel.modDirVersion}";
in
stdenv.mkDerivation {
  name = "dscud-${version}-${kernel.version}";
  src = fetchurl {
    url = "ftp://dscclient:diamondproject@111.93.197.227/DSC_Client/RMM1616/Linux/UD7.0.0_64bit/V0.1/${archive}";
    sha256 = "159vblk35iwdp7ddpzk9x0flhqqabgra32mz82117pr99ywffyfj";
  };

  hardeningDisable = [ "pic" ];

  buildInputs = [ unzip ];

  patches = [
    ./linux-4.x.patch
    ./pkgconfig.patch
  ];

  buildPhase = ''
    make KDIR=${kd}/build KERNEL=LINUX_30
  '';

  installPhase = ''
    binDir="$out/lib/modules/${kernel.modDirVersion}/misc"
    mkdir -p "$binDir"
    cp -v dscudkp.ko "$binDir"
    mkdir -p $out/bin
    cp ${./init_dscudkp} $out/bin/init_dscudkp
    chmod a+x $out/bin/init_dscudkp
    substituteInPlace $out/bin/init_dscudkp \
        --replace /bin/bash ${bash}/bin/bash
    mkdir -p $out/lib/udev/rules.d
    cat > $out/lib/udev/rules.d/91-dscudkp.rules <<-EOF
    SUBSYSTEM=="module", DEVPATH=="/module/dscudkp", RUN+="$out/bin/init_dscudkp"
    EOF
    mkdir -p $out/lib $out/include $out/share/pkgconfig
    cp -v libdscud-${version}.a $out/lib
    cp -v dscud.h dscudkp.h $out/include
    substitute dscud.pc.in $out/share/pkgconfig/dscud.pc \
        --replace @PREFIX@ $out
  '';

  meta = {
    description = "Universal driver for Diamond data-acquisition boards";
    homepage = "http://www.diamondsystems.com/products/ud70";
    license = stdenv.lib.licenses.bsd2;
    platform = stdenv.lib.platforms.linux;
  };
}
