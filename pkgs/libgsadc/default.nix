{ stdenv, fetchurl, pkgconfig, libtool
, glib
, libuuid
, gs24dsi16wrc-dev ? null
, gs24dsi-dev ? null
, noWRC ? false
}:

assert (!noWRC) -> (gs24dsi16wrc-dev != null && gs24dsi-dev == null);
assert (noWRC) -> (gs24dsi-dev != null && gs24dsi16wrc-dev == null);

let
  version = "1.3.14";
  tarball = "release-${version}.tar.gz";
  repo = "uwaploe/gsadc";
in
stdenv.mkDerivation {
  name = "libgsadc-${version}";
  src = fetchurl {
    url = "https://bitbucket.org/${repo}/get/${tarball}";
    sha256 = "0z1f9v96dlp6mplp5xs2p929vz2dmhm734cycvzaxgxbclka4bw1";
  };
  nativeBuildInputs = [ pkgconfig ];

  buildInputs =
    [ libtool glib libuuid ]
    ++ stdenv.lib.optionals noWRC [ gs24dsi-dev ]
    ++ stdenv.lib.optionals (!noWRC) [ gs24dsi16wrc-dev ];

  propagatedBuildInputs =
    [ glib libuuid ]
    ++ stdenv.lib.optionals noWRC [ gs24dsi-dev ]
    ++ stdenv.lib.optionals (!noWRC) [ gs24dsi16wrc-dev ];

  preBuild = ''
    make clean
    buildFlagsArray=("CFLAGS=-Wall -O3 -msse2 -mfpmath=sse")
  '' + stdenv.lib.optionalString noWRC ''
    makeFlagsArray=("BOARD=24dsi")
  '';

  installPhase = ''
    make "''${makeFlagsArray[@]}" prefix=$out install-lib
   '';

  meta = {
    description = "Application library for General Standards A/D boards";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}
